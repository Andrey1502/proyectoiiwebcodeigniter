<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cliente extends CI_Controller {

	function __construct(){

		parent::__construct();
		$this->load->database();
	
      }

      public function index()
      {
        
        $this->load->view("ClienteMain");
      }

    
      
      /**
       * Llama al método que carga el catálogo de productos ofrecido por la empresa
       */
      public function cargarCatalogoProductos(){

        $infoCategorias = $this->ClienteModel->getCategories();


        $this->load->view("CatalogoProductos",array('infoCategorias' => $infoCategorias));

      }

     
      
            
      /**
       * Añade al carrito del cliente el identificador del producto seleccionado
       * @param  mixed $idProducto Identificador del producto a añadir al carrito
       */
      public function annadirAlCarrito(int $idProducto){

           $idCliente = $this->session->userdata('id');

           $cantidadRequeridaPorDefecto = 1;



           if($this->ClienteModel->annadirCarrito($idCliente,$idProducto,$cantidadRequeridaPorDefecto)){
            
            $this->cargarCatalogoProductos();
            
           }


    }
    
    /**
     * Obtiene el carrito de compras del cliente que se encuentra en sesión
     */
    public function cargarCarritoCompras(){

      $idCliente = $this->session->userdata('id');


      $datosCarrito = $this->ClienteModel->cargarCarritoCliente($idCliente);


    //Obtener productos asociados al carrito
    $productos = array();

    $cantidad = array();
          
      foreach($datosCarrito as $row){

        array_push($productos,$this->ClienteModel->cargarProductosCarrito($row->id_producto));

        array_push($cantidad,$row->cantidad_requerida);

      }


      $this->load->view("CarritoCompras",array('productos' => $productos,'cantidad' => $cantidad));

    }
    
    /**
     * Llama al método que elimina del carrito el registro seleccionado por el cliente 
     * @param  mixed $idProducto Identificador del producto a eliminar del carrito del cliente
     */
    public function eliminarProductoCarrito(int $idProducto){

        $idCliente = $this->session->userdata('id');

        if($this->ClienteModel->eliminarProductoCarrito($idCliente,$idProducto)){
          $this->cargarCarritoCompras();
        }


    }
    
        
    /**
     * Método que procede a guardar la compra de los productos restantes en el carrito del cliente en sesión
     */
    public function comprarProductos(){

      //1-Actualizar el restante de los productos solicitados en la compra


      $productosSeleccionados = $this->input->post("idProductos");

      $cantidadProducto = $this->input->post("cantidadProducto");

      $this->actualizarRestante($productosSeleccionados,$cantidadProducto);


       

        //2- Registrar la compra en la base de datos

        $idCompra = $this->insertarCompra();

       //3- Registrar los productos involucrados en la compra


        $idCliente = $this->session->userdata('id');


        $comprasCliente = $this->ClienteModel->obtenerRegistrosClienteCarrito($idCliente);
        
        
      $i=0;

      $totalCompra = 0;


      while($i < $comprasCliente){

        $idProducto = $productosSeleccionados[$i];

        $infoProducto = array($this->ClienteModel->getProductById($idProducto));

        $montoProducto = 0;
        
        foreach($infoProducto as $pro){
          $montoProducto = $pro->precio;

        }

        
        $cantidadRequerida = $cantidadProducto[$i];
        
        if($this->ClienteModel->registrarProductosCompra($idCompra,$idProducto,$montoProducto,$cantidadRequerida)){

          $totalCompra+= $montoProducto * $cantidadRequerida;  

            $i++;

          

        }
          



    }
    
    //4- Actualizar el total de la compra

    $this->ClienteModel->actualizarTotalCompra($idCompra,$totalCompra);
      
      $idCliente = $this->session->userdata('id');

      //5- Eliminar los registros del cliente en el carrito

      if($this->ClienteModel->eliminarRegistrosCarritoCliente($idCliente)){
        $this->cargarCarritoCompras();

      }
     
    }
        
    /**
     * Actualizar el restante de los productos según la cantidad solicitada por el cliente
     * @param  mixed $productosSeleccionados Productos a actualizar restante
     * @param  mixed $cantidadProducto Cantidad a restar del restante.
     */
    private function actualizarRestante(array $productosSeleccionados,array $cantidadProducto){

      $idCliente = $this->session->userdata('id');
      
        

      $registrosCliente = $this->ClienteModel->obtenerRegistrosClienteCarrito($idCliente);

      $producto = array();

      $cantidadEditar;


    $i=0;

    while($i < $registrosCliente){

       $idProducto = $productosSeleccionados[$i];

      $cantidadEditar = $cantidadProducto[$i];


      
      array_push($producto,$this->ClienteModel->getProductById($idProducto));

      foreach($producto as $row){
        $restante = $row->restante;

        $cantidadActualizar= $restante - $cantidadEditar;



      }
      
      $this->ClienteModel->actualizarRestanteProducto($idProducto,$cantidadActualizar);

   $i++;
    
      }
    

   }

   
   
   /**
    * Registrar la compra del cliente   
    */
   private function insertarCompra(){

    $idCliente = $this->session->userdata('id');


       return $this->ClienteModel->registrarCompra($idCliente);

   }

   /**
    *Cargar las compras del cliente en sesión, luego de obtenerlas se envían a la vista que las mostrará   
    */
   public function verCompras(){
     
    $idCliente = $this->session->userdata('id');

     $info = $this->ClienteModel->obtenerComprasCliente($idCliente);

     $this->load->view("VerCompras",array('info' => $info));
   }

   /**
    *Cargar los productos relacionados a la compra que el cliente seleccionó y carga la vista que los mostrará.   
    */
    
    public function verDetalle(int $idCompraVisualizar){

       $productos = array();

       $cantidades = array();
     
       $info = $this->ClienteModel->obtenerDetalleCompraCliente($idCompraVisualizar);


       for($i=0;$i  < count($info);$i++){

            array_push($productos,$this->ClienteModel->getProductById($info[$i]->id_producto));

            array_push($cantidades,$info[$i]->cantidad_requerida);


       }


       $this->load->view("VerDetalle",array('productos' => $productos,'cantidades'=>$cantidades));



       

   }

}