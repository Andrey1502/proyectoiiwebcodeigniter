<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RegistroModel extends CI_Model{

    function __construct(){
        parent:: __construct();
        $this->load->database();
     }

         
     /**
      *Verifica que el nombre de usuario que se está proporcionando para el registro no se encuentra ya ocupado
      *
      * @param  mixed $nombreUsuario Nombre de usuario a verificar
      * @return Cantidad de usuarios con ese nombre, de no haber ninguno el retorno de la función será de cero
      */
     public function verificarExistenciaNombreUsuario(string $nombreUsuario){
        
        $query = $this->db->get_where('usuarios', array('nombre_usuario' => $nombreUsuario));

        if($query){
            return $this->db->affected_rows();

        }

     

    
       

      
     }
    
    /**
     * Inserta un nuevo cliente en la base de datos
     *
     * @param  mixed $nombre Nombre del cliente
     * @param  mixed $primerApellido Primer apellido del cliente
     * @param  mixed $segundoApellido Segundo Apellido del cliente
     * @param  mixed $telefono Teléfono del cliente
     * @param  mixed $correo Correo del cliente
     * @param  mixed $direccion Direccion del cliente
     * @param  mixed $nombreUsuario Nombre de usuario del cliente
     * @param  mixed $contrasenna Coontraseña del cliente
     * @return true si lo guarda, false si no
     */
    public function guardar(string $nombre,string $primerApellido,string $segundoApellido,string $telefono,string $correo,
    string $direccion,string $nombreUsuario,string $contrasenna){

        
         $query = $this->db->insert("usuarios",array("nombre"=> $nombre,"primer_apellido"=> $primerApellido,"segundo_apellido"=>$segundoApellido,
         "telefono"=>$telefono,"correo"=>$correo,"direccion"=>$direccion,"contrasenna"=>$contrasenna,"privilegio"=>'0',
         "nombre_usuario"=>$nombreUsuario ));

            if ($query) {
                return true;
             } else {
                return false;
            }

        }

    }