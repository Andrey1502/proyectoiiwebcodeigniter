<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminModel extends CI_Model{

    function __construct(){
        parent:: __construct();
        $this->load->database();
    }  
    
    /**
     * Obtiene la categoría con el nombre enviado
     * @param  mixed $nombreCategoria Nombre de la categoría a obtener
     * @return Categoría con el nombre indicado
     */
    public function obtenerCategoriaSeleccionada(string $nombreCategoria){
      return $this->db->where('nombre', $nombreCategoria)->get('categorias')->row();



    }

    /**
     * Obtiene el nombre la categoría con el identificador enviado.
     * @param  mixed $id Identificador de la categoría a obtener
     * @return Categoría con el identificador indicado
     */

    public function obtenerNombreCategoria(int $id){
      return $this->db->where('id', $id)->get('categorias')->row();
        
    }

    /**
     * Obtiene la categoría con el identificador enviado
     * @param  mixed $idCategoria Identificador de la categoría a obtener
     * @return Categoría con el identificador indicado
     */
    public function obtenerCategoriaSeleccionadaId(int $idCategoria){
      $query = $this->db->get_where('categorias', array('id' => $idCategoria));
    
      if ($query->result()) {
          return $query->result();
        } else {
          return false;
        }

}

/**
     * Obtiene el producto con el identificador enviado
     * @param  mixed $idProducto Identificador del producto.
     * @return Producto con el identificador enviado
*/

public function obtenerProductoSeleccionadoId(int $idProducto){
  $query = $this->db->get_where('productos', array('id' => $idProducto));

  if ($query->result()) {
      return $query->result();
    } else {
      return false;
    }

}
     
     /**
      * Obtiene todos los productos de la base de datos
      * @return Productos registrados en el sistema
      */
     public function getAllProducts(){
       $query =  $this->db->query("SELECT * FROM productos");

       if ($query->result()) {
        return $query->result();
      } else {
        return false;
      }
    }

     /**
      * Obtiene todas las categorías de la base de datos
      * @return Categorías registradas en el sistema
      */

     public function getCategories(){
       $query =  $this->db->query("SELECT * FROM categorias");

       if ($query->result()) {
        return $query->result();
      } else {
        return false;
      }
    }

    
    /**
     * Registra un nuevo producto en la base de datos
     *
     * @param  mixed $nombre Nombre del producto
     * @param  mixed $descripcion Descripción del producto
     * @param  mixed $imagen Imagen del producto
     * @param  mixed $categoria Categoría del producto
     * @param  mixed $restante Restante del producto
     * @param  mixed $precio Precio del producto
     * @return true si lo inserta, false si no
     */
    public function guardarProducto(string $nombre,string $descripcion,string $imagen,int $categoria,int $restante, int $precio){

        $query = $this->db->insert("productos",array("nombre"=> $nombre,"descripcion"=> $descripcion,"imagen"=>$imagen,
         "categoria"=>$categoria,"restante"=>$restante,"precio"=>$precio));

            if ($query) {
                return true;
             } else {
                return false;
            }
    }
    
    /**
     * Editar un producto de la base de datos
     *
     * @param  mixed $id Identificador del producto a editar
     * @param  mixed $nombre Nombre del producto
     * @param  mixed $descripcion Descripción del producto
     * @param  mixed $imagen Imagen del producto
     * @param  mixed $categoria Categoría del producto
     * @param  mixed $restante Restante del producto
     * @param  mixed $precio Precio del producto
     * @return true si lo actualiza, false si no
     */
    public function editarProducto(int $id,string $nombre,string $descripcion,string $imagen,int $categoria,int $restante, int $precio){

       $this->db->where('id', $id);

       $data  = array( 
        'nombre'=> $nombre,
        'descripcion'=> $descripcion,
        'imagen'=>$imagen,
        'categoria'=>$categoria,
        'restante'=>$restante,
        'precio'=>$precio
        
    );

    if($this->db->update('productos',$data)){
        return true;
    }

    else{
      return false;
    }

        
  }
    
    /**
     * Elimina un producto de la base de datos
     *
     * @param  mixed $id Identificador del producto a eliminar
     * @return true si lo elimina, false si no
     */
    public function eliminarProducto(int $id){
		
        $this->db->where('id', $id);
        if($this->db->delete('productos')){
          return true;
        }

        else{
          return false;
        }

    
  }

  /**
   * Agrega una nueva categoría al sistema 
   *
   * @param  mixed $nombre Nombre de la nueva categoría
   * @return true si se guarda correctamente, false si no
   */
  public function agregarCategoria(string $nombre){
    $query = $this->db->insert("categorias",array("nombre"=> $nombre));

            if ($query) {
                return true;
             } else {
                return false;
            }
  }
  
  /**
   * Actualiza una categoría de la base de datos
   *
   * @param  mixed $id Identificador de la categoría a editar
   * @param  mixed $nombre Nombre nuevo de la categoría
   * @return true si la edita, false si no
   */
  public function actualizarCategoria(int $id,string $nombre){
    $this->db->where('id', $id);

    $data  = array( 
        'nombre' => $nombre
        
    );

    if($this->db->update('categorias',$data)){
      return true;
    }

    else{
      return false;
    }
  }
  
  /**
   * Valida que la categoría que se desea eliminar no posea productos asociados
   *
   * @param  mixed $idCategoria Identificador de la categoría a verificar
   * @return cantidad de filas afectadas en la validación
   */
  public function validarProductosCategoria(int $idCategoria){
    $query = $this->db->get_where('productos', array('categoria' => $idCategoria));

        if($query){
            return $this->db->affected_rows();

        }

  }
  
  /**
   *Elimina la categoría seleccionada por el usuario
   *
   * @param  mixed $id Identificador de la categoría a eliminar
   * @return true si la elimina, false si no
   */
  public function eliminarCategoria(int $id){
		
    $this->db->where('id', $id);
    if($this->db->delete('categorias')){
      return true;
    }

    else{
      return false;
    }


}

 /**
*Verifica que el nombre del producto que se está proporcionando no se encuentre ya registrado
*
* @param  mixed $nombreProducto Nombre del producto a verificar
* @return Cantidad de productos con ese nombre, de no haber ninguno el retorno de la función será de cero
*/
public function verificarExistenciaNombreProducto(string $nombreProducto){
        
  $query = $this->db->get_where('productos', array('nombre' => $nombreProducto));

  if($query){

      return $this->db->affected_rows();

  }




 


}

 /**
*Verifica que el nombre de la categoría que se está proporcionando no se encuentre ya registrado
*
* @param  mixed $nombreCategoria Nombre de la categoría a verificar
* @return Cantidad de categorías con ese nombre, de no haber ninguno el retorno de la función será de cero
*/
      public function verificarExistenciaNombreCategoria(string $nombreCategoria){
        
        $query = $this->db->get_where('categorias', array('nombre' => $nombreCategoria));

        if($query){

            return $this->db->affected_rows();

        }

     

    
       

      
}


/**
 * Obtiene la cantidad de clientes registrados en el sistema
 *
 * @return Cantidad de filas afectadas en la consulta
 */
public function obtenerClientesRegistrados(){
  $query =  $this->db->query("SELECT * FROM usuarios WHERE privilegio !=1");

       if ($query->result()) {
        return $this->db->affected_rows();

      } else {
        return false;
      }
}

/**
 * Obtiene la cantidad de productos vendidos en la empresa
 *
 * @return Cantidad de productos vendidos en la empresa
 */
public function obtenerCantidadProductosVendidos(){
  $this->db->select_sum('cantidad_requerida');
    $this->db->from('productos_compra');
    $query = $this->db->get();
    return $query->row();
}

/**
 * Obtiene el monto total de las ventas en la empresa
 *
 * @return Monto total de ventas de la empresa
 */
public function obtenerMontoTotalVentas(){
  $this->db->select_sum('total');
    $this->db->from('compras');
    $query = $this->db->get();
    return $query->row();
}

}