<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php
error_reporting(0);

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Login</title>

	

	<style type="text/css">

	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body {
		margin: 0 15px 0 15px;
	}

	p.footer {
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}

	#container {
		margin: 10px;
		border: 1px solid #D0D0D0;
		box-shadow: 0 0 8px #D0D0D0;
	}
	</style>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.2/css/bulma.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/CSS/estiloLogin.css">
	 <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inicio se sesión</title>
    
</head>

<body class="cuerpoApp">

<div id="container">

	<div id="body">








    <!--Sección encabezado de la página-->
    <section class="hero is-small is-primary is-bold">
        <div class="hero-body">
            <div class="container">
                <h1 class="encabezadoPrincipal column">Sistema de Tiendas Online</h1>

                <h1 class="seccionActual column">Inicio de sesión</h1>
            </div>
        </div>
    </section>


    <!--Sección para pedir los datos requeridos del usuario-->



    <section class="">
        <div class="container">
            <div class="">
                <div class="">
                    <div class="form-group">

                        <form  method="POST" action="/Programacion-Web/Proyecto-2/proyectoiiwebcodeigniter/index.php/LoginCont/validar">
                            <div class="form-group">
                                <input type="text" class="input is-primary is-rounded" id="nombreUsuario" type="text"
                                    placeholder="Usuario" name="nUsu" required>
                            </div>

                            <div class="form-group">
                                <input class="input is-primary is-rounded" id="contrasenna" type="password"
                                    placeholder="Contraseña" name="passUsu" required>
                            </div>


                            <div class="form-group">
                                <button type="submit" name="iniciar" class="button is-success is-normal" class="iniciarSesion">Iniciar
                                    Sesión</button>
                            </div>


                        </form>
                    </div>
                </div>




            </div>


        </div>

    </section>


    <section class="section">
        <div class="container">
            <div class="columns">
                <div class="column">
                    <a type="rel" href="/Programacion-Web/Proyecto-2/proyectoiiwebcodeigniter/index.php/Registro">¿No tienes una cuenta?,<strong>Regístrate
                            ahora!!</strong></a>
                </div>
            </div>
        </div>
    </section>







</body>

</html>