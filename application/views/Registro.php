<?php
error_reporting(0);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registrarse al sistema</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.2/css/bulma.min.css" />
    
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/CSS/styleRegistro.css">


</head>
<body class="cuerpoApp">
    <!--Sección encabezado de la página-->
    <section class="hero is-small is-info is-bold">
        <div class="hero-body">
            <div class="container">
                <h1 class="encabezadoPrincipal">Sistema de Tienda Online</h1>

                <h1 class="seccionActual">Registrar una cuenta</h1>
            </div>
        </div>
    </section>

    <!--Sección para pedir los datos requeridos del usuario-->
    <section class="section">
        <div class="container">
            <div class="">
            <div class="">
                <form action="/Programacion-Web/Proyecto-2/proyectoiiwebcodeigniter/index.php/Registro/registrar" method="post">
                    <div class="datos form-group">
            
                        <div class="form-group">
                            <input class="input is-primary is-rounded" id="nombreCompletoUsuario" placeholder="Nombre" type="text" name="nombre" required>
                        </div>

                        <div class="form-group">
                            <input class="input is-primary is-rounded" placeholder="Primer Apellido" type="text" name="primerApellido" required>
                        </div>

                        <div class="form-group">
                            <input class="input is-primary is-rounded" placeholder="Segundo Apellido" type="text" name="segundoApellido" required>
                        </div>

                        <div class="form-group">
                            <input class="input is-primary is-rounded" placeholder="Teléfono" type="text" name="telefono" required>
                        </div>


                        <div class="form-group">
                            <input class="input is-primary is-rounded" id="correoUsuario" type="email" name="correo" placeholder="Correo" required>
                        </div>

                        <div class="form-group">
                            <input class="input is-primary is-rounded" placeholder="Direccion" type="text" name="direccion" required>
                        </div>

                       <div class="form-group">
                            <input class="input is-primary is-rounded" id="usuario" placeholder="Nombre de Usuario" type="text" name="nombreUsuario" required>
                        </div>
                    
                        <div class="form-group">
                            <input class="input is-primary is-rounded" id="contrasenna" placeholder="Contraseña" type="password" name="contrasenna" required>
                        </div>
                            
                      
                        <div>
                            
                            <button id="botonEnviar" type="submit" class="Registrar button is-success is-normal">Registrar</button>
                    
                        </div>
            
                </form>

            </div>
                
            </div> 
               

        </div>

            
      

    </section>
    
    
        
    <section class="section">
        <div class="container">
            <div class="columns">
                <div class="column"> 
                    <a href="/Programacion-Web/Proyecto-2/proyectoiiwebcodeigniter/">¿Ya tienes una cuenta?,<strong>Inicia sesión!!</strong> </a>
                </div>
              </div>
        </div>
      </section>
  


       

    
    
    
</body>
</html>



