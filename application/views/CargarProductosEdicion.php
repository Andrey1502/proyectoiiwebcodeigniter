<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require ('header.php');
if($this->session->userdata('logeado')){

    if($this->session->userdata('privilegio')!=1){
          redirect(site_url("Cliente"));
    }


}

else{
    
    redirect(site_url("LoginCont"));
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Editar Producto</title>

	<style type="text/css">

	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body {
		margin: 0 15px 0 15px;
	}

	p.footer {
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}

	#container {
		margin: 10px;
		border: 1px solid #D0D0D0;
		box-shadow: 0 0 8px #D0D0D0;
	}
	</style>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.2/css/bulma.min.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/CSS/estiloH.css">

</head>
<body>

<div id="container">

	<div id="body">

	<table class= "table is-bordered is-striped is-narrow is-hoverable is-fullwidth">

    <tr>
     <th>Nombre  </th>
     <th>Descripción  </th>
    <th>Imagen  </th>
    <th>Categoría  </th>
    <th>Restante  </th>
    <th>Precio  </th>
    <th>Acciones</th>

	

	</tr>

	<?php
				foreach($info as $row)
				{
                    
                   $cateSeleccionada = $this->AdminModel->obtenerNombreCategoria($row->categoria);
                    
				   $nombreCategoria = "";


				   
				   if($cateSeleccionada->nombre !=null){
                       $nombreCategoria = $cateSeleccionada->nombre;
				   }
                    

					echo "<tr>".
                    "<td>".$row->nombre."</td>".
                    "<td>".$row->descripcion."</td>".
                    "<td>". '<img src = "'.$row->imagen.'" width="100" height="100"></img>'."</td>".
                    "<td>".$nombreCategoria."</td>".
                    "<td>".$row->restante."</td>".
                    "<td>".$row->precio."</td>".
				    "<td><a href='/Programacion-Web/Proyecto-2/proyectoiiwebcodeigniter/index.php/Admin/obtenerProductoSeleccionado/".$row->id."/$nombreCategoria'>Modificar</a></td>".
					
					"</tr>";
				}
			?>





	</table>

	<div>
<a href="/Programacion-Web/Proyecto-2/proyectoiiwebcodeigniter/index.php/Admin">Volver a la página principal</a>

</div>
	
	</div>

	
	


</div>


</body>
</html>