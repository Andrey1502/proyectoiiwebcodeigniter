<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if($this->session->userdata('logeado')){

    if($this->session->userdata('privilegio')!=1){
          redirect(site_url("Cliente"));
    }


}

else{
    
    redirect(site_url("LoginCont"));
}
?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Página de administradores</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/CSS/estiloM.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/CSS/estiloH.css">



</head>

<body class="cuerpoApp">

<?php require ('header.php') ?>

 
<div class="estadisticas">

    <?php

    //Obtener clientes registrados en el sistema
     
    $clientesRegistrados = $this->AdminModel->obtenerClientesRegistrados();

    //Obtener cantidad de productos vendidos

    $cantidadProductosVendidos = 0;
    $resultado = array($this->AdminModel->obtenerCantidadProductosVendidos());

    foreach($resultado as $can){
        $cantidadProductosVendidos = $can->cantidad_requerida;

    }







//Obtener monto total de las ventas

$sumaTotal = 0;

$sumaProductosVendidos = array($this->AdminModel->obtenerMontoTotalVentas());

foreach($sumaProductosVendidos as $total){
    $sumaTotal = $total->total;
}

?>
   <div class="clientes">
   <label>Clientes registrados:</label>
   
    <label><?php echo $clientesRegistrados;?></label>
    </div>

    <div class="productosVendidos">
    <label>Cantidad de productos vendidos:</label>
    <label><?php echo $cantidadProductosVendidos?></label>

    </div>

    <div class="totalVentas">
    <label>Monto total de ventas:</label>
    <label><?php echo $sumaTotal?></label>

    </div>
    


   
</div>


</body>
</html>

